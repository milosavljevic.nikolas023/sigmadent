<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatibile" content="IE-edge">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <title>Stomatoloska Ordinacija SigmaDent</title>

        <!-- font awesome cdn link  -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">

        <!-- bootstrap cdn link  -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/4.6.1/css/bootstrap.min.css">

        <!-- custom css file link  -->
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>

        <header class="header fixed-top">
            
            <div class="row align-items-center justify-content-between">

                <a href="#home" class="logo">sigma<span>Dent</span></a>

                <nav class="nav">
                    <a href="#home">Pocetna strana</a>
                    <a href="#works">Nasi radovi</a>
                    <a href="#about">O nama</a>
                    <a href="#services">Usluge</a>
                    <a href="#reviews">Iskustva pacijenata</a>
                    <a href="#contact">Kontakt</a>
                </nav>

                <a href="#contact" class="link-btn">Zakazite pregled</a>

                <div id="menu-btn" class="fas fa-bars"></div>

            </div>

        </header>

        <!-- home section -->

        <section class="home" id="home">

            <div class="row min-vh-100 align-items-center">
                <div class="content">
                    <h3>ulepsacemo vam osmeh</h3>
                    <p>Tekst o nama pa je sirok pa dukacak, a ima i sta da se kaze. Sve u svemu, mazi kremu.</p>
                    <a href="#contact" class="link-btn">zakazite sastanak</a>
                </div>
            </div>

        </section>

        <!-- js file link -->
        <script src="js/script.js"></script>
    </body>
</html>